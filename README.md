# Project Title, Description

## Infos A

- lorem
- ipsum
- foo
- bar

## Infos B

### Beispiel Code-Snippets

```css

:root {
    font-size: 100%;
    line-height: 1.3;
}
```

### Colors

#### Text

- text `#202020`
- link `#393FFA`
- hover `#573DFF`
- button `#ffffff`

#### Buttons

- grün `#23AA01`
- rot `#fa0900`
- grau `#6C6C6C`
- blau1 `#1844FF`
- blau2 `#3523C5`

## Images are possible in Markdown, of course

![](oval.png)
